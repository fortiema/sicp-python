# Structure and Implementation of Computer Programs (SICP) - Python #
This repository is a collective of the assignments and projects for the course CS61A at UC Berkeley.

---

### Completed Projects ###
* P1 - Pigs Game
* P2 - Twitter Trends

### WIP Projects ###

### ToDo Projects ###
* P3 - Ants
* P4 - Logo